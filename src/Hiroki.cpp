

#include <Box2D/Box2D.h>
#include <Corrade/Containers/GrowableArray.h>
#include <Corrade/Utility/Arguments.h>
#include <Magnum/GL/Context.h>
#include <Magnum/GL/DefaultFramebuffer.h>
#include <Magnum/GL/Buffer.h>
#include <Magnum/GL/Mesh.h>
#include <Magnum/GL/Renderer.h>
#include <Magnum/Math/ConfigurationValue.h>
#include <Magnum/Math/DualComplex.h>

#include <Magnum/MeshTools/Compile.h>
#include <Magnum/Platform/Sdl2Application.h>
#include <Magnum/Primitives/Circle.h>
#include <Magnum/Primitives/Square.h>
#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/SceneGraph/TranslationRotationScalingTransformation2D.h>
#include <Magnum/SceneGraph/Scene.h>
#include <Magnum/Shaders/Flat.h>
#include <Magnum/Trade/MeshData.h>


namespace Magnum { namespace Examples {

typedef SceneGraph::Object<SceneGraph::TranslationRotationScalingTransformation2D> Object2D;
typedef SceneGraph::Scene<SceneGraph::TranslationRotationScalingTransformation2D> Scene2D;

using namespace Math::Literals;

struct InstanceData {
    Matrix3 transformation;
    Color4 color;
};


class HirokiDrawable;

struct UserData {
	Object2D *circleObject;
	Object2D *squareObject;
	Deg opinion;
	Int flipcounter;
	size_t index;
};



class MouseQueryCallback : public b2QueryCallback
{
public:
	MouseQueryCallback(const b2Vec2& point) {
		_point = point;
		_fixture = NULL;
	}

	bool ReportFixture(b2Fixture* fixture) override {
		b2Body* body = fixture->GetBody();
		if (body->GetType() == b2_dynamicBody) {
			bool inside = fixture->TestPoint(_point);
			if (inside) {
				_fixture = fixture;
				// We are done, terminate the query.
				return false;
			}
		}

		// Continue the query.
		return true;
	}

	b2Vec2 _point;
	b2Fixture* _fixture;
};





class Box2DExample: public Platform::Application {
    public:
        explicit Box2DExample(const Arguments& arguments);

    private:

		// physics parameter
        const Float _boxlen   = 100.f; // size of the containing box
        const Float _size     = 2.3f; // radius of a bubble
		const unsigned int _N = 200;  // number of bubbles
		Float _network  = 08.0f;      // size of AABB in multiple of _size
		
		// model parameter
		// const Float _c = 0.0001f;     // strength of social conformity
		// const Float _epsilon = 0.0f;  // stochastic fluctuation term for nodes� ideas
		const Float _h = 0.2f;        // homophily
		const Float _a = 0.0f;        // attention to novelty
		const Float _theta_h = 0.15f; // homophily threshold, when does a opinion get repellant
		// const Float _theta_a = 0.0f;  // attention to novelty threshold

		void drawEvent() override;

		Color4 circleColor(Deg opinion) {
			return Color4::fromHsv({ opinion, 0.5f, 0.75f }, .70f);
		}
		Color4 squareColor(Deg opinion) {
			return Color4::fromHsv({ opinion, 0.5f, 0.99f }, .25f);
		}

		b2MouseJoint* _mouseJoint = NULL;
		b2Body* _groundBody = NULL;

		void mousePressEvent(MouseEvent& event) override;
		void mouseReleaseEvent(MouseEvent& event) override;
		void mouseMoveEvent(MouseMoveEvent& event) override;
		void mouseScrollEvent(MouseScrollEvent& event) override;

		b2Body* createBody(Object2D& object, // creates a bubble
			const Float size,
			const DualComplex& transformation,
			Float density = 1.0f, Float restitution = 0.0f);
		b2Body* createEdge(Object2D& object, // for the aquarium
			const Vector2& a,
			const Vector2& b,
			const DualComplex& transformation,
			Float density = 1.0f, Float restitution = 0.0f);

		Float random() {
			return static_cast <Float> (rand()) / static_cast <Float> (RAND_MAX);
		}

		// bubbles
		GL::Mesh _meshCircle{ NoCreate };
		GL::Buffer _circleInstanceBuffer{ NoCreate };
		Containers::Array<InstanceData> _circleInstanceData;

		// AABBs
		GL::Mesh _meshSquare{ NoCreate };
		GL::Buffer _squareInstanceBuffer{ NoCreate };
		Containers::Array<InstanceData> _squareInstanceData;

		Shaders::Flat2D _shader{ NoCreate };

		Scene2D _scene;
		Object2D* _cameraObject;
		SceneGraph::Camera2D* _camera;
		SceneGraph::DrawableGroup2D _drawables;
		Containers::Optional<b2World> _world;
		const Float _zoom0 = 2.0f;
		Float _zoom = _zoom0;
};


// report neighbors in AABB
class NeighborCallback : public b2QueryCallback {

public:
	b2Body* _body;
	Float sumweight = 0;
	NeighborCallback(b2Body* b) :b2QueryCallback() { _body = b; }
	std::vector<b2Body*> neighbors;

	// we keep calculation of lai out of this loop, to allow
	// more sophisticated methods of circular mean calculations

	bool ReportFixture(b2Fixture* fixture) {
		b2Body* found = fixture->GetBody();
		// exclude borders, and myself
		if (found->GetType() == b2_dynamicBody && found != _body) {
			neighbors.push_back(found);
			b2Vec2 dist = found->GetPosition() - _body->GetPosition();
			Float w = 2 / sqrt(dist.Length()); // this is arbitrary
			sumweight += w;
		}
		return true; // keep going to find all fixtures in the query area
	}
};


// to keep things in flow, add a little spin when touching borders
class ContactListener : public b2ContactListener {
public:
	const Float _av = 0.1f;      // repelling impulse
	b2Vec2 center = b2Vec2{ 0.0f, 0.0f }; // _boxlen / 2.f


	void BeginContact(b2Contact* contact) { (void)contact; }
	void EndContact(b2Contact* contact)	{ (void)contact; }
	void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) { (void)contact; (void)impulse; }

	void PreSolve(b2Contact* contact, const b2Manifold* oldManifold) {
		(void)oldManifold; // suppress unused warning

		b2Body* a = contact->GetFixtureA()->GetBody();
		b2Body* b = contact->GetFixtureB()->GetBody();

		if (a->GetType() == b2_staticBody) {
			b2Vec2 diff = center - b->GetPosition();
			b->ApplyLinearImpulse(_av*diff, b->GetPosition(), true);
		}
		else if (b->GetType() == b2_staticBody) {
			// changed roles
			b2Vec2 diff = center - a->GetPosition();
			a->ApplyLinearImpulse(_av*diff, a->GetPosition(), true);
		}
	}
};


// used for both circles and squares
class HirokiDrawable: public SceneGraph::Drawable2D {
    public:
        explicit HirokiDrawable(Object2D& object, 
				 Containers::Array<InstanceData>& instanceData, 
			     const Color4& color, 
			     SceneGraph::DrawableGroup2D& drawables): 
					SceneGraph::Drawable2D{object, &drawables}, 
					_instanceData(instanceData), 
					_color{color} {}

		void setColor(const Color4& color) {
			_color = color;
		}

    private:
        void draw(const Matrix3& transformation, SceneGraph::Camera2D&) override {
            arrayAppend(_instanceData, Containers::InPlaceInit,
                transformation, _color);
        }

        Containers::Array<InstanceData>& _instanceData;
        Color4 _color;
};

b2Body* Box2DExample::createBody(Object2D& object, // part of user dataa
	const Float size,
	const DualComplex& transformation,
	const Float density, Float restitution) {

	b2BodyDef bodyDefinition;
	bodyDefinition.position.Set(transformation.translation().x(), transformation.translation().y());
	bodyDefinition.angle = Float(transformation.rotation().angle());
	bodyDefinition.type = b2_dynamicBody;
	b2Body* body = _world->CreateBody(&bodyDefinition);

	b2CircleShape shape;
	shape.m_radius = size;

	b2FixtureDef fixture;
	fixture.friction = 0.2f;
	fixture.density = density;
	fixture.restitution = restitution;

	fixture.shape = &shape;
	body->CreateFixture(&fixture);

	// setup user data
	UserData* ud = new UserData; // hello memory leak
	ud->opinion = random() * 360.0_degf;
	ud->flipcounter = 0;

	ud->circleObject = &object; 
	ud->circleObject->setScaling({ size, size });

	ud->squareObject = new Object2D{ &_scene }; // FIXME
	ud->squareObject->setScaling({ size*_network, size*_network });

	body->SetUserData(ud);

    return body;
}


b2Body* Box2DExample::createEdge(Object2D& object, // used?
                                 const Vector2& a,
                                 const Vector2& b,
                                 const DualComplex& transformation,
                                 Float density,
                                 Float restitution) {

    b2BodyDef bodyDefinition;
    bodyDefinition.position.Set(transformation.translation().x(), transformation.translation().y());
    bodyDefinition.angle = Float(transformation.rotation().angle());
    b2Body* body = _world->CreateBody(&bodyDefinition);

    b2EdgeShape shape;
    shape.SetTwoSided(b2Vec2(a.x(), a.y()), b2Vec2(b.x(), b.y()));

    b2FixtureDef fixture;
    fixture.friction = 0.8f;
    fixture.density = density;
    fixture.restitution = restitution;
    fixture.shape = &shape;
    body->CreateFixture(&fixture);

    body->SetUserData(&object);

    return body;
}


Box2DExample::Box2DExample(const Arguments& arguments): Platform::Application{arguments, NoCreate} {

	/* Make it possible for the user to have some fun */
	/* Make it possible for the user to have some fun */
    Utility::Arguments args;
    args.addOption("network", "4").setHelp("network", "size how large the social network of a person is")
		.addOption("zoom", std::to_string(_zoom0)).setHelp("zoom", "initial zoom factor")
        .addSkippedPrefix("magnum", "engine-specific options")
        .parse(arguments.argc, arguments.argv);

    _network = args.value<Float>("network");
	_zoom = args.value<Float>("zoom");


    /* Try 8x MSAA, fall back to zero samples if not possible. Enable only 2x
       MSAA if we have enough DPI. */
    {
        const Vector2 dpiScaling = this->dpiScaling({});
        Configuration conf;
        conf.setTitle("Hiroki")
            .setSize(conf.size(), dpiScaling);
        GLConfiguration glConf;
        glConf.setSampleCount(dpiScaling.max() < 2.0f ? 8 : 2);
        if(!tryCreate(conf, glConf))
            create(conf, glConf.setSampleCount(0));

    }

    /* Configure camera */
    _cameraObject = new Object2D{&_scene};

    _camera = new SceneGraph::Camera2D{*_cameraObject};
    _camera->setAspectRatioPolicy(SceneGraph::AspectRatioPolicy::Extend)
        .setProjectionMatrix(Matrix3::projection({_boxlen*_zoom, _boxlen*_zoom })) // squares are outside of the box
        .setViewport(GL::defaultFramebuffer.viewport().size());
	
    /* Create the Box2D world free of gravity */
    _world.emplace(b2Vec2{0.0f, 0.0f});
	_world->SetContactListener(new ContactListener());
	
	GL::Renderer::enable(GL::Renderer::Feature::Blending);
	GL::Renderer::setBlendFunction(
		GL::Renderer::BlendFunction::SourceAlpha, /* or One for premultiplied */
		GL::Renderer::BlendFunction::OneMinusSourceAlpha);
	GL::Renderer::setLineWidth(0.8f);

    /* Create an instanced shader */
    _shader = Shaders::Flat2D{
        Shaders::Flat2D::Flag::VertexColor|
        Shaders::Flat2D::Flag::InstancedTransformation};

    /* Circle mesh with an (initially empty) instance buffer */
    _meshCircle = MeshTools::compile(Primitives::circle2DSolid(16)); // 16 segments
    _circleInstanceBuffer = GL::Buffer{};
    _meshCircle.addVertexBufferInstanced(_circleInstanceBuffer, 1, 0,
        Shaders::Flat2D::TransformationMatrix{},
        Shaders::Flat2D::Color4{});

	/* Same for the square mesh with another instance buffer */
	_meshSquare = MeshTools::compile(Primitives::squareWireframe()); 
	_squareInstanceBuffer = GL::Buffer{};
	_meshSquare.addVertexBufferInstanced(_squareInstanceBuffer, 1, 0,
		Shaders::Flat2D::TransformationMatrix{},
		Shaders::Flat2D::Color4{});

	const Float bhalf = _boxlen / 2.0f;

    /* Create the 4 sides of the box */
	for (std::size_t s = 0; s < 4; s++) {
		auto ground = new Object2D{ &_scene };
		switch (s) {
		case 0:
			_groundBody = createEdge(*ground, { -bhalf, -bhalf }, { bhalf, -bhalf },
				DualComplex::translation(Vector2::yAxis(0.0f)));			
			break;
		case 1:
			createEdge(*ground, { bhalf, -bhalf }, { bhalf, bhalf },
				DualComplex::translation(Vector2::yAxis(0.0f)));
			break;
		case 2:
			createEdge(*ground, { bhalf, bhalf }, { -bhalf, bhalf },
				DualComplex::translation(Vector2::yAxis(0.0f)));
			break;
		case 3:
			createEdge(*ground, { -bhalf, bhalf }, { -bhalf, -bhalf },
				DualComplex::translation(Vector2::yAxis(0.0f)));
			break;
		}
	}

    /* Create randomly located bubbles, and squares */
    for (unsigned int i = 0; i < _N; i++) {
        auto bubble = new Object2D{&_scene};
		// random placement
        const DualComplex transformation = DualComplex::translation(
            {(random()-0.5f) * (_boxlen-1), (random()-0.5f) * (_boxlen-1)});
        b2Body* b = createBody(*bubble, _size, transformation, 0.2f, 0.8f);
		UserData ud = (*static_cast<UserData*>(b->GetUserData()));
		new HirokiDrawable{ *bubble, _circleInstanceData, circleColor(ud.opinion), _drawables};
		ud.index = i;

		if (_network > 0.0f) {
			new HirokiDrawable{ *ud.squareObject, _squareInstanceData, squareColor(ud.opinion), _drawables };
		}

    }


    setSwapInterval(1);
    #if !defined(CORRADE_TARGET_EMSCRIPTEN) && !defined(CORRADE_TARGET_ANDROID)
    setMinimalLoopPeriod(16);
    #endif
}




void Box2DExample::drawEvent() {
	GL::Renderer::setClearColor(0x060600_rgbf);  
    GL::defaultFramebuffer.clear(GL::FramebufferClear::Color);
	//Utility::Debug{} << "min: " << range.min() << " max: " << range.max();

    /* Step the world and update all object positions */
    _world->Step(1.0f/60.0f, 8, 4);
	for (b2Body* body = _world->GetBodyList(); body; body = body->GetNext()) {

		// only bubbles are to be rendered
		if (body->GetType() != b2_dynamicBody)
			continue;

		UserData ud = (*static_cast<UserData*>(body->GetUserData()));
		b2Vec2 pos = body->GetPosition();

		// move objects
		Object2D *circleObject = ud.circleObject;
		circleObject->setTranslation({ pos.x, pos.y })
			.setRotation(Complex::rotation(Rad(body->GetAngle())));

		Object2D *squareObject = ud.squareObject;
		squareObject->setTranslation({ pos.x, pos.y }); // no rotation here

		// find neighbors
		Float ndist = _size * _network;
		NeighborCallback queryCallback(body);
		b2AABB aabb;
		aabb.lowerBound = b2Vec2{ pos.x - ndist, pos.y - ndist };
		aabb.upperBound = b2Vec2{ pos.x + ndist, pos.y + ndist };
		_world->QueryAABB(&queryCallback, aabb);

		size_t nn = queryCallback.neighbors.size();

		b2Vec2 f = b2Vec2{ 0,0 };

		for (size_t i = 0; i < nn; i++) {
			b2Body* nb = queryCallback.neighbors[i];
			b2Vec2 npos = nb->GetPosition();
			UserData nud = (*static_cast<UserData*>(nb->GetUserData()));
			Deg nop = nud.opinion;
			Deg diff = ud.opinion - nop;
			if (diff > 180.0_degf)
				diff -= 360.0_degf;
			if (diff < -180.0_degf)
				diff += 360.0_degf;

			Float Fa = 0; // abs(lai - nb.userData.opinion) - self.theta_a

			Float Fh = _theta_h - (abs(diff) / 180.0_degf);
			Float attr = _h * Fh + _a * Fa;

			// calc force
			b2Vec2 dist = (pos - npos);
			f = f + (attr / dist.Length()) * dist;
		}
		if (nn > 0)
			body->ApplyLinearImpulse(-1.f / nn * f, pos, true);

#if 0
			// change my opinion according to the neighbors
			dop = self.c * (lai - op) + self.epsilon*random()

			if self.renderer and self.renderer.stepcounter > 1500 and random() < 0.00002:
			b.userData.opinion += 0.5 + 0.3*random() # flip
				b.userData.flipcounter = 100
				else:
			b.userData.opinion += dop
				if b.userData.opinion > 1:
			b.userData.opinion -= 1
#endif
		
	}

    /* Populate instance data with transformations and colors */
    arrayResize(_circleInstanceData, 0);
	arrayResize(_squareInstanceData, 0);
    _camera->draw(_drawables);

    /* Upload instance data to the GPU and draw everything in a single call */
    _circleInstanceBuffer.setData(_circleInstanceData, GL::BufferUsage::DynamicDraw);
    _meshCircle.setInstanceCount(_circleInstanceData.size());

	_squareInstanceBuffer.setData(_squareInstanceData, GL::BufferUsage::DynamicDraw);
	_meshSquare.setInstanceCount(_squareInstanceData.size());
 
	_shader.setTransformationProjectionMatrix(_camera->projectionMatrix());
	_shader.draw(_meshCircle);
	_shader.draw(_meshSquare);

    swapBuffers();
    redraw();
}


// ---- mouse handling --------------------

void Box2DExample::mousePressEvent(MouseEvent& event) {
	if (event.button() != MouseEvent::Button::Left) return;
	if (_mouseJoint != NULL) return;

	/* Calculate mouse position in the Box2D world. Make it relative to window,
	with origin at center and then scale to world size with Y inverted. */
	const auto mv = Vector2{ event.position() } / Vector2{ windowSize() };
	const auto position
		= _camera->projectionSize() * Vector2::yScale(-1.0f) * (mv - Vector2{ 0.5f, .5f });

	// same thing in box format
	b2Vec2 p = b2Vec2{ position.x(), position.y() };

	// Make a small box.
	b2AABB aabb;
	b2Vec2 d;
	d.Set(0.001f, 0.001f);
	aabb.lowerBound = p - d;
	aabb.upperBound = p + d;

	// Query the world for overlapping shapes.
	MouseQueryCallback callback(p);
	_world->QueryAABB(&callback, aabb);

	if (callback._fixture)
	{
		float frequencyHz = 5.0f;
		float dampingRatio = 0.7f;

		b2Body* body = callback._fixture->GetBody();
		b2MouseJointDef jd;
		jd.bodyA = _groundBody;
		jd.bodyB = body;
		jd.target = p;
		jd.maxForce = 1000.0f * body->GetMass();
		b2LinearStiffness(jd.stiffness, jd.damping, frequencyHz, dampingRatio, jd.bodyA, jd.bodyB); 
		
		_mouseJoint = static_cast <b2MouseJoint*>(_world->CreateJoint(&jd));
		body->SetAwake(true);
	}
}


void Box2DExample::mouseReleaseEvent(MouseEvent& event) {
	(void)event; // supress warning
	if (_mouseJoint) {
		_world->DestroyJoint(_mouseJoint); // snap
		_mouseJoint = NULL;
	}
}


void Box2DExample::mouseMoveEvent(MouseMoveEvent &event) {
	if (!event.buttons()) return;
	if (_mouseJoint) {
		/* Calculate mouse position in the Box2D world. Make it relative to window,
		with origin at center and then scale to world size with Y inverted. */
		const auto mv = Vector2{ event.position() } / Vector2{ windowSize() };
		const auto position
			= _camera->projectionSize() * Vector2::yScale(-1.0f) * (mv - Vector2{ 0.5f, .5f });

		// convert to box format
		b2Vec2 p = b2Vec2{ position.x(), position.y() };
		_mouseJoint->SetTarget(p);

		event.setAccepted();
		//redraw(); /* camera has changed, redraw! */ TODO
	}
}

void Box2DExample::mouseScrollEvent(MouseScrollEvent& event) {
	const Float delta = event.offset().y();
	if (Math::abs(delta) < 1.0e-2f) return;
	_zoom += delta/50.f;
	_camera->setProjectionMatrix(Matrix3::projection({ _boxlen*_zoom, _boxlen*_zoom}));

	event.setAccepted();
	redraw(); /* camera has changed, redraw! */
}



		/* change color, to be removed here
		UserData *ud = (static_cast<UserData*>(body->GetUserData()));
		ud->opinion += 180.0_degf; // flip
		if (ud->opinion > 360.0_degf)
			ud->opinion -= 360.0_degf;
		Object2D *obj = ud->circleObject;
		for (SceneGraph::AbstractFeature2D& feature : obj->features()) {
			HirokiDrawable *drw = static_cast<HirokiDrawable*>(&feature);
			drw->setColor(circleColor(ud->opinion));
		} 

		*/





}}

MAGNUM_APPLICATION_MAIN(Magnum::Examples::Box2DExample)
