REM setup emscripten ath
cd C:\dev\repos\emsdk
emsdk_env.bat


REM build box2d lib
cd C:\dev\repos\box2d\build
cmake -DBOX2D_BUILD_DOCS=OFF -DBOX2D_BUILD_TESTBED=OFF -DBOX2D_BUILD_UNIT_TESTS=OFF -DCMAKE_BUILD_TYPE=Debug  -DCMAKE_TOOLCHAIN_FILE=C:\dev\workspace\magnum-bootstrap-base\magnum\toolchains\generic\Emscripten-wasm.cmake  -G Ninja ..
 
emmake cmake --build .

REM build magnum wasm
cd C:\dev\workspace\hiroki\build-emscripten
rm CMakeCache.txt

cmake .. -DBOX2D_LIBRARY=C:\dev\repos\box2d\build\src\box2d.bc -DBOX2D_INCLUDE_DIR=C:\dev\repos\box2d\include -DCMAKE_TOOLCHAIN_FILE="../magnum/toolchains/generic/Emscripten-wasm.cmake" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=c:/dev/exe -DCORRADE_RC_EXECUTABLE=C:\dev\exe\x86-windows\tools\corrade\corrade-rc.exe -DEMSCRIPTEN_PREFIX=C:/dev/repos/emsdk -GNinja

cd C:\dev\workspace\hiroki\build-emscripten
cmake --build .  




