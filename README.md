#### Extreme Ideas emerging...

A visual interpretation of the model proposed by Hiroki Sayama 
in [Extreme Ideas Emerging from Social Conformity and Homophily: An Adaptive Social Network Model](https://www.mitpressjournals.org/doi/pdf/10.1162/isal_a_00349).

The code is based on the [Magnum Box2d example](https://doc.magnum.graphics/magnum/examples-box2d.html), and is
cross-compiled to WebAssembly with [emscripten](https://emscripten.org/). For build instructions, see the 
magnum documentation [build with emscripten](https://doc.magnum.graphics/magnum/building.html#building-cross-emscripten).

Licence: https://github.com/mosra/magnum/blob/master/COPYING

See it running at http://hollyhook.de/hiroki/.


